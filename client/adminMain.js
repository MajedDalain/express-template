
const Vue = require('vue');
const VueRouter = require('vue-router');

Vue.use(VueRouter); 

var router = new VueRouter({
    routes: [
      { path: '/', component: require("./components/adminHome.vue") },
      { path: '/recipes/:recipeId/', component: require("./components/adminShowRecipe.vue") },
      { path: '/createRecipe', component: require("./components/adminCreateRecipe.vue") },
      { path: '/createComment/:recipeId', component: require("./components/adminCreateComment.vue") },
      { path: '/editRecipe/:recipeId', component: require("./components/editRecipe.vue") },
      { path: '/editcomment/:recipeId/:commentId', component: require("./components/editComment.vue") }
    ]
  });  


  var baserouter = require("./components/adminBaserouter.vue");




  new Vue({
    router: router,
    render: function (createElement) {
      return createElement(baserouter);
    }
  }).$mount("#mountpoint");
const Vue = require('vue');
const VueRouter = require('vue-router');

Vue.use(VueRouter);

var router = new VueRouter({
  routes: [
   { path: '/', component: require("./components/userHome.vue") },
   { path: '/recipes/:recipeId/', component: require("./components/userShowInfo.vue")  },
   { path: '/userAddRecipe', component: require("./components/userAddRecipe.vue") },
   { path: '/createComment/:recipeId', component: require("./components/userCreateComment.vue") }
  ]
});

// this vue component will act as the root for our SPA
var baserouter = require("./components/userBaseRouter.vue");

// and finally, mount the view-model instance
new Vue({
  router: router,
  render: function (createElement) {
    return createElement(baserouter);
  }
}).$mount("#mountpoint");
//The mount command tells Vue where to place the template content (in HTML element with id mountpoint)